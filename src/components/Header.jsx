import React from 'react';
import logo from '../logo.png';


function Header() {
    return (

        <div>
            <h1>Bienvenido a E-comerce GAPSI</h1>
            <img src={logo}/>
        </div>
    );
}
export default Header;