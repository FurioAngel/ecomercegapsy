import React, {useState} from 'react';
import Header from './components/Header';
import Producto from './components/Producto';
import Usuario from './components/Usuario';
import Carrito from './components/Carrito';
import './gapsi.css'
import Articulo from './components/Articulo';

// Inicia aplicación principal
function App() {

    const [usuario, guardaUser] = useState({});
    const [siguiente, guardaSiguiente] = useState(false);
    const [articulo, guardaArticulo] = useState([]);
    const [inicia, guardaInicia] = useState(true);

    // Funcion que carga el Usuario principal
    const iniciaApp = () => {

        const resUsuario = fetch('https://node-red-nxdup.mybluemix.net/visitor', {"method": "POST"});
        const usuarioRes = resUsuario.then(respuesta => respuesta.json());
        usuarioRes.then(res => guardaUser(res.data));

    }

    // Funcion que renderiza la lista de productos
    const funcionSiguiente = () => {
        console.log('funcionSiguiente');
        guardaSiguiente(true);

        // Funcion que regresa los productos
        const resultado = fetch('https://node-red-nxdup.mybluemix.net/productos/reloj/2');
        const productos = resultado.then(respuesta => respuesta.json());
        productos.then(res => guardaArticulo(res.data.products));

    }


    return (
        <div className="centro">
            <button onClick={iniciaApp}>
                Iniciar aplicación
            </button>

            <Header/> {
            siguiente ? (
                <div>
                    <Carrito/> {

                    articulo.map(art => (


                        <Articulo articulo={art}/>
                    ))
                } </div>
            ) : (

                <div>
                    <Usuario usuario={usuario}/>
                    <button onClick={funcionSiguiente}>
                        Continuar
                    </button>
                </div>
            )
        } </div>
    );
}

export default App;
